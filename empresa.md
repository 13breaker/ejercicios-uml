#Empresa

``` mermaid
classDiagram

        Persona <|-- Empleado
        Persona <|-- Cliente
        Empleado <|-- Directivo
        Empresa *-- Empleado
        Empresa o-- Cliente
        Directivo -- Empleado

    class Empresa{
        Nombre
    }
    class Directivo{
        -Categoria
        -Subordinados
        +mostrar() void
    }
    class Persona{
        -Nombre: String
        -Edad: Integer
        +mostrar() void
    }
        class Empleado{
            -Sueldo bruto
            +mostrar()
            +calcular_salario()
        }
        class Cliente{
            -Telefono contacto
            +mostrar()

        }